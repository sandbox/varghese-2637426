CONTENTS OF THIS FILE
---------------------

 * Introduction
 * Requirements
 * Installation
 * Credits

INTRODUCTION
------------

This module provides Livestream handler for video embed field module now with 
this module you can add embed videos from http://livestream.com to your website.
You can use livestream standard web page address for video url, for example
http://original.livestream.com/allthingsfun

Module also can get thumbnail image

Can work using filters and tokens in text field.


REQUIREMENTS
============

- Video Embed Field
  https://drupal.org/project/video_embed_field

INSTALLATION
------------

1. Install and enable the module as usual.
2. Navigate to "admin/config/media/vet_video_styles" 
and configure some video style.
3. For "Video tokens" - Enable "Video Embedding" filter at page
/admin/config/content/formats/full_html , or for other formats, and make sure
that filter is before filter "Convert URLs into links"

Configuration
-------------

All settings for this module are on the Video Embed Livestream configuration page, under the
Configuration section, in the player style settings. You can visit the
configuration page directly at admin/config/media/vef/vef_video_styles.

CREDITS
-------
* Audrius Vaitonis (deepapple)
* BrightLemon - We create communities that increase engagement 
and participation - http://brightlemon.com
