<?php

/**
 * @file
 * Adds a handler for livestream videos to Video Embed Field.
 *
 * @see video_embed_field.api.php
 */

/**
 * Implements hook_video_embed_handler_info().
 */
function video_embed_livestream_video_embed_handler_info() {
  $handlers = array();
  $handlers['livestream'] = array(
    'title' => 'Live Stream Video',
    'function' => 'video_embed_livestream_handle_video',
    'thumbnail_function' => 'video_embed_livestream_handle_thumbnail',
    'thumbnail_default' => drupal_get_path('module', 'video_embed_livestream') . '/img/livestream_logo.png',
    'form' => 'video_embed_livestream_form',
    'form_validate' => 'video_embed_livestream_field_handler_livestream_form_validate',
    'domains' => array(
      'livestream.com',
      'original.livestream.com',
    ),
    'defaults' => array(
      'width' => 640,
      'height' => 360,
    ),
  );
  return $handlers;
}

/**
 * Defines the form elements for the livestream videos configuration form.
 *
 * @param array $defaults
 *   The form default values.
 *
 * @return array
 *   The provider settings form array.
 */
function video_embed_livestream_form(array $defaults) {
  $form = array();
  $form['width'] = array(
    '#type' => 'textfield',
    '#title' => t('Player Width'),
    '#description' => t('The width of the player.'),
    '#default_value' => $defaults['width'],
  );
  $form['height'] = array(
    '#type' => 'textfield',
    '#title' => t('Player Height'),
    '#description' => t('The height of the player.'),
    '#default_value' => $defaults['height'],
  );
  return $form;
}

/**
 * Validates the form elements for the livestream video configuration form.
 *
 * @param array $element
 *   The form element to validate.
 * @param array $form_state
 *   The form to validate state.
 * @param array $form
 *   The form to validate structure.
 */
function video_embed_livestream_field_handler_livestream_form_validate(array $element, array &$form_state, array $form) {
  video_embed_field_validate_dimensions($element);
}

/**
 * Handler for livestream videos.
 *
 * @param string $url
 *   The video URL.
 * @param array $settings
 *   The settings array.
 *
 * @return array|bool
 *   The video iframe, or FALSE in case the ID can't be retrieved from the URL.
 */
function video_embed_livestream_handle_video($url, array $settings) {
  $id = _video_embed_livestream_get_video_id($url);
  if ($id) {
    $video = array(
      '#theme' => 'video_embed_livestream',
      '#url' => 'http://cdn.livestream.com/embed/' . $id . '?layout=4',
      '#width' => $settings['width'],
      '#height' => $settings['height'],
    );
    return $video;
  }
  return FALSE;
}

/**
 * Gets the thumbnail url for livestream videos.
 *
 * @param string $url
 *   The video URL.
 *
 * @return array
 *   The video thumbnail information.
 */
function video_embed_livestream_handle_thumbnail($url) {
  $id = _video_embed_livestream_get_video_id($url);
  $image_url = video_embed_livestream_get_thumbnail_url($id);
  if ($image_url) {
    return array(
      'id' => $id,
      'url' => $image_url,
    );
  }
  return FALSE;
}

/**
 * Helper to get image url, downloads embed iframe content and parses it.
 *
 * @param string $id
 *   The video ID.
 *
 * @return array|bool
 *   The video thumbnail url, or FALSE in case something failes
 */
function video_embed_livestream_get_thumbnail_url($id) {
  return 'http://thumbnail.api.livestream.com/thumbnail?name=' . $id;
}

/**
 * Helper function to get the livestream video's id. We support current urls.
 *
 * @param string $url
 *   The video URL.
 *
 * @return string|bool
 *   The video ID, or FALSE in case the ID can't be retrieved from the URL.
 */
function _video_embed_livestream_get_video_id($url) {
  $matches = array();
  preg_match('/livestream.com\/(.*)/', $url, $matches);
  if ($matches && !empty($matches[1])) {
    // Security check, this is the only place where we use user input.
    $match = explode('?', $matches[1]);
    return check_plain($match[0]);
  }
  // Otherwise return FALSE.
  return FALSE;
}

/**
 * Implements hook_theme().
 *
 * Using video_embed_livestream.tpl.php as template.
 */
function video_embed_livestream_theme() {
  return array(
    'video_embed_livestream' => array(
      'template' => 'video_embed_livestream',
      'variables' => array(
        'url' => NULL,
        'width' => NULL,
        'height' => NULL,
      ),
    ),
  );
}
