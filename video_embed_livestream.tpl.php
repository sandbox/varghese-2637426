<?php

/**
 * @file
 * Theme implementation for video_embed_livestream.
 */
?>
<iframe src="<?php print $url; ?>" width="<?php print $width; ?>" height="<?php print $height; ?>" frameborder="0" scrolling="no" webkitAllowFullScreen mozallowfullscreen allowFullScreen></iframe>
